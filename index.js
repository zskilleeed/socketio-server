const express = require('express');
const path = require('path');
const app = express();

app.set('port', process.env.PORT || 3000);

app.use(express.static(path.join(__dirname, 'public')));


const server = app.listen(app.get('port'), () => {

    console.log(`server on port ${app.get('port')}`)

});

const socket = require('socket.io');
const io = socket(server);

io.on('connection', () =>{
console.log("new coinnection");

})

